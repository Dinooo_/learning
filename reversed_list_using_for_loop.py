input = 1, 2, 3, 4 #user input

number = []

for num in input[::-1]:
    number.append(num)

print("ČÍSLA V LISTU -", number)
print("ČÍSLA MIMO LIST -", *number)
print("S ČÁRKAMA -", end= " ")
print(*number, sep=", ")